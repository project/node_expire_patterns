<?php

/**
 * @file
 * Rules module integration.
 */

use Drupal\node_expire_patterns\Module\Hook\RulesHookHandler;

/**
 * Implements hook_rules_event_info().
 *
 * @ingroup rules
 */
function node_expire_patterns_rules_event_info() {
  return RulesHookHandler::hookRulesEventInfo();
}
