<?php

/**
 * @file
 * ModuleUtils class.
 */

namespace Drupal\node_expire_patterns\Module\Utils;

use Drupal\node_expire\Module\CommonExpire\Utils\CommonUtils as CommonUtilsBase;

/**
 * ModuleUtils class.
 */
class ModuleUtils {

  /**
   * Makes Weight options array for Form UI.
   */
  public static function getWeightOptions() {
    $arr = array();

    for ($i = -15; $i <= 15; $i++) {
      $arr[$i] = $i;
    }

    return $arr;
  }

  /*
   * Cleans up redundant plain variables.
   */
  public static function doVariablesCleanup() {
    CommonUtilsBase::doVariablesCleanupByTemplate('node_expire_patterns_container%');
    CommonUtilsBase::doVariablesCleanupByTemplate('node_expire_patterns_enabled%');
    CommonUtilsBase::doVariablesCleanupByTemplate('node_expire_patterns_action%');
  }

  /**
   * Utility function to display debug info.
   *
   * Display debug info and write it into log.
   *
   * @param object $data
   *   Data to display.
   */
  public static function debug($data = array()) {
    drupal_set_message(filter_xss_admin('<pre>' . print_r($data, TRUE) . '</pre>'));

    // https://www.drupal.org/node/86124
    // If you need to print an array, it can be done like so:
    // watchdog('debug', '<pre>'. print_r($view, TRUE) .'</pre>');
    watchdog('node_expire_patterns', '<pre>' . print_r($data, TRUE) . '</pre>');
  }

  /**
   * Utility function to display debug info.
   *
   * Display debug info.
   *
   * @param object $data
   *   Data to display.
   */
  public static function debugScreen($data = array()) {
    drupal_set_message(filter_xss_admin('<pre>' . print_r($data, TRUE) . '</pre>'));
  }

  /**
   * Utility function to display debug info.
   *
   * Write debug info into log.
   *
   * @param object $data
   *   Data to display.
   */
  public static function debugLog($data = array()) {

    // https://www.drupal.org/node/86124
    // If you need to print an array, it can be done like so:
    // watchdog('debug', '<pre>'. print_r($view, TRUE) .'</pre>');
    watchdog('node_expire_patterns', '<pre>' . print_r($data, TRUE) . '</pre>');
  }

}
