<?php

/**
 * @file
 * DbHandler class.
 */

namespace Drupal\node_expire_patterns\Module\Db;

use Drupal\node_expire_patterns\Module\Actions\Action;

/**
 * DbHandler class.
 */
class DbHandler {

  /**
   * Returns expired node_expire records.
   */
  public static function selectForAction(Action $action, $timestamp) {
    // TODO: Move timestamp outside to have it the same in tracking records. (DONE)
    // TODO: Change for node_expire_patterns.

//    $result = db_query('SELECT n.nid FROM {node} n
//      JOIN {node_expire} ne ON n.nid = ne.nid
//      WHERE ne.expire <= :ne_expire',
//      array(':ne_expire' => $timestamp));

    // https://api.drupal.org/comment/56008
    // https://www.drupal.org/node/1439650

    // Create a subquery, which is just a normal query object.
    $subquery = db_select('node_expire_patterns_actions', 'a');
    $subquery->fields('a', array('nid', 'action_uuid'));
    $subquery->condition('action_uuid', $action->uuid);
    // TODO: Add condition by action_status later.

    $query = db_select('node', 'n');
    $query->join('node_expire', 'ne', 'n.nid = ne.nid');
    $query->leftJoin($subquery, 'a', 'n.nid = a.nid');
    $query->fields('n', array('nid'));
    $query->condition('a.action_uuid', NULL);
    $query->condition('ne.expire', $timestamp, '<=');
    $result = $query->execute()->fetchAllAssoc('nid');

//     watchdog('debug', '<pre>'. print_r($result, TRUE) .'</pre>');


    // https://www.drupal.org/node/86124
    // If you need to print an array, it can be done like so:
    // watchdog('debug', '<pre>'. print_r($view, TRUE) .'</pre>');

    return $result;
  }

  /**
   * Inserts Action Record.
   */
  public static function insertActionRecord(Action $action, $nid, $status) {
    db_insert('node_expire_patterns_actions')
      ->fields(array(
        'nid' => $nid,
        'node_type' => $action->nodeType,
        'action_order' => $action->number,
        'action_weight' => $action->weight,
        'action_uuid' => $action->uuid,
        'action_name' => $action->name,
        'action_type' => $action->type,
        'action_timestamp' => REQUEST_TIME,
        'action_tag' => $action->tag,
        'action_status' => $status,
      ))
      ->execute();
  }

  /**
   * Returns expired node_expire records.
   */
  public static function selectActionsByNode($nid) {

    $query = db_select('node_expire_patterns_actions', 'a');
    $query->fields('a');
    $query->condition('nid', $nid);
    $result = $query->execute()->fetchAllAssoc('aid');

//    watchdog('debug', '<pre>'. print_r($result, TRUE) .'</pre>');

    return $result;
  }

  /**
   * Sets node as expired by $nid.
   *
   * @param int $nid
   *   Node ID that should set the expired flag.
   */
  public static function setExpired($nid) {
    db_update('node_expire')
      ->fields(
        array(
          'expired' => 1,
          'lastnotify' => REQUEST_TIME,
        )
      )
      ->condition('nid', $nid)
      ->condition(db_or()
        ->condition('lastnotify', 0)
        ->condition('expired', 1, '!='))
      ->execute();
  }

  /**
   * Deletes node_expire record by $nid.
   *
   * Used in hook_node_delete() implementation.
   */
  public static function deleteNodeExpire($nid) {
    db_delete('node_expire')
      ->condition('nid', $nid)
      ->execute();
  }

  /**
   * Returns count of node_expire record by $nid.
   */
  public static function getNodeExpireCountByNid($nid) {
    $cnt = db_query(
      'SELECT count(nid)
      FROM {node_expire} ne
      WHERE ne.nid = :nid',
      array(':nid' => $nid))->fetchField();

    return $cnt;
  }

  /**
   * Writes node_expire record.
   */
  public static function writeRecord($node_expire, $nid) {
    // Check, is it insert or update.
    $cnt = self::getNodeExpireCountByNid($nid);

    // Write the record.
    if ($cnt == 0) {
      // Insert.
      drupal_write_record('node_expire', $node_expire);
    }
    else {
      // Update.
      drupal_write_record('node_expire', $node_expire, 'nid');
    }
  }

  /**
   * Returns expired node_expire records.
   */
  public static function selectExpired() {
    $result = db_query('SELECT n.nid FROM {node} n
      JOIN {node_expire} ne ON n.nid = ne.nid
      WHERE ne.expire <= :ne_expire',
      array(':ne_expire' => REQUEST_TIME));

    return $result;
  }

  /**
   * Returns expired node_expire records where expired flag is not set.
   */
  public static function selectExpiredNonFlagged() {
    $result = db_query('SELECT n.nid FROM {node} n
      JOIN {node_expire} ne ON n.nid = ne.nid
      WHERE ne.expire <= :ne_expire AND ne.expired = 0',
      array(':ne_expire' => REQUEST_TIME));

    return $result;
  }

  /**
   * Returns node_expire records for hook_node_load().
   */
  public static function selectForNodeLoad($node_expire_enabled) {
    $result = db_query(
      'SELECT n.nid, n.type, expire, expired, lastnotify
       FROM {node} n
         INNER JOIN {node_expire} ne
           ON n.nid = ne.nid
       WHERE n.nid
         IN (:node_expire_enabled)',
      array(':node_expire_enabled' => $node_expire_enabled));

    return $result;
  }

}
