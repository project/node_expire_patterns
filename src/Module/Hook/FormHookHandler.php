<?php

/**
 * @file
 * FormHookHandler class.
 */

namespace Drupal\node_expire_patterns\Module\Hook;

use Drupal\node_expire\Module\CommonExpire\Actions\ActionsHelper;
use Drupal\node_expire\Module\Config\ConfigHandler as ConfigHandlerBase ;
use Drupal\node_expire\Module\Utils\TimestampUtils;
use Drupal\node_expire_patterns\Module\Actions\ActionsHandler;
use Drupal\node_expire_patterns\Module\Actions\NodeAction;
use Drupal\node_expire_patterns\Module\Config\NodeTypeConfigHandler;
use Drupal\node_expire_patterns\Module\Utils\ModuleUtils;

/**
 * FormHookHandler class.
 */
class FormHookHandler {

  /**
   * Implements hook_form_node_type_form_alter().
   *
   * Enable/Disable expiration feature on node types
   */
  public static function hookFormNodeTypeFormAlter(&$form, &$form_state) {
    if (user_access('administer node expire')) {
      $ntypes = variable_get('node_expire_patterns_ntypes', array());
      $node_type  = $form['#node_type']->type;
      $handler = new NodeTypeConfigHandler($node_type);

      $form['workflow']['node_expire_container']['node_expire_patterns_enabled'] = array(
        '#title' => t('Enable Node Expire Patterns'),
        '#description' => t('Allow nodes to have some patterns (chains) of expiry events.'),
        '#type' => 'checkbox',
        '#default_value' => empty($ntypes[$node_type]['enabled']) ? '' : $ntypes[$node_type]['enabled'],
      );

      // Visibility.
      $states = array(
        'visible' => array(
          ':input[name="node_expire_patterns_enabled"]' => array('checked' => TRUE),
        ),
      );

      $form['workflow']['node_expire_container']['node_expire_patterns_container'] = array(
        '#type' => 'fieldset',
        '#title' => t('Node Expire Patterns'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#states' => $states,
      );

      $form['workflow']['node_expire_container']['node_expire_patterns_container']['node_expire_patterns_actions_count'] = array(
        '#type'          => 'select',
        '#title'         => t('Actions count'),
        '#default_value' => $handler->getNodeTypeActionsCount(),
        '#options'       => array(
          0 => t('0 (None)'),
          1 => t('1'),
          2 => t('2'),
          3 => t('3'),
          4 => t('4'),
          5 => t('5'),
          6 => t('6'),
          7 => t('7'),
          8 => t('8'),
          9 => t('9'),
          10 => t('10'),
        ),
        '#description'   => t('Actions count.'),
        '#states' => $states,
      );

      $states1 = self::makeStates(1);
      self::makeActionElements($handler, 1, $form, $states1);

      $states2 = self::makeStates(2);
      self::makeActionElements($handler, 2, $form, $states2);

      $states3 = self::makeStates(3);
      self::makeActionElements($handler, 3, $form, $states3);

      $states4 = self::makeStates(4);
      self::makeActionElements($handler, 4, $form, $states4);

      $states5 = self::makeStates(5);
      self::makeActionElements($handler, 5, $form, $states5);

      $states6 = self::makeStates(6);
      self::makeActionElements($handler, 6, $form, $states6);

      $states7 = self::makeStates(7);
      self::makeActionElements($handler, 7, $form, $states7);

      $states8 = self::makeStates(8);
      self::makeActionElements($handler, 8, $form, $states8);

      $states9 = self::makeStates(9);
      self::makeActionElements($handler, 9, $form, $states9);

      $states10 = self::makeStates(10);
      self::makeActionElements($handler, 10, $form, $states10);

      // Add special validate/submit functions.
      // Referenced in the bottom of node_expire.module
      // module_load_include('ntype.inc', 'node_expire');
      $form['#validate'][] = '_node_expire_patterns_form_node_type_form_alter_validate';
      $form['#submit'][] = '_node_expire_patterns_form_node_type_form_alter_submit';
    }
  }

  /*
   * Makes #states element for particular action number.
   */
  private static function makeStates($num) {

    $arr = array();
    for ($i = 0; $i <= $num-1; $i++) {
      $arr[] = array('value' => $i);
    }

    // Visibility variable.
    $states = array(
      'invisible' => array(
        ':input[name="node_expire_patterns_actions_count"]' => $arr,
      ),
    );

    return $states;
  }

  /*
   * Makes UI elements for action with particular number.
   */
  private static function makeActionElements(
    NodeTypeConfigHandler $handler, $num, &$form, &$states) {

    $name = 'action' . $num;
    $name_ui = t('Action ') . $num;
    $uuid = $handler->getNodeTypeActionUuid($num);

    $form['workflow']['node_expire_container']['node_expire_patterns_container'][$name] = array(
      '#type'  => 'fieldset',
      '#title' => $name_ui,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Action UUID ') . $uuid,
      '#states' => $states,
    );
//    $form['workflow']['node_expire_container']['node_expire_patterns_container'][$name]['node_expire_patterns_' . $name . '_markup'] = array(
//      '#markup' => t('Action UUID ') . $uuid,
//      '#states' => $states,
//    );
    $form['workflow']['node_expire_container']['node_expire_patterns_container'][$name]['node_expire_patterns_' . $name . '_uuid'] = array(
      '#title' => t('UUID'),
      '#description' => t("UUID"),
//      '#type' => 'textfield',
//      '#type' => 'hidden',
      '#type' => 'value',
//      '#disabled' => TRUE,
      '#size' => 60,
      '#default_value' => $uuid,
      '#states' => $states,
    );
    $form['workflow']['node_expire_container']['node_expire_patterns_container'][$name]['node_expire_patterns_' . $name . '_name'] = array(
      '#title' => t('Name'),
      '#description' => t("Action's name"),
      '#type' => 'textfield',
      '#size' => 60,
      '#default_value' => $handler->getNodeTypeActionName($num),
      '#states' => $states,
    );
    $options_w = ModuleUtils::getWeightOptions();
    $form['workflow']['node_expire_container']['node_expire_patterns_container'][$name]['node_expire_patterns_' . $name . '_weight'] = array(
      '#type'          => 'select',
      '#title'         => t('Weight'),
      '#default_value' => $handler->getNodeTypeActionWeight($num),
      '#options'       => $options_w,
      '#description'   => t("Action's weight."),
      '#states' => $states,
    );
    $form['workflow']['node_expire_container']['node_expire_patterns_container'][$name]['node_expire_patterns_' . $name . '_interval'] = array(
      '#title' => t('Interval'),
      '#description' => t('Interval in relation to Expiry date.'),
      '#type' => 'textfield',
      '#size' => 60,
      '#default_value' => $handler->getNodeTypeActionInterval($num),
      '#states' => $states,
    );
    $options = ActionsHelper::getActionOptions();
    $form['workflow']['node_expire_container']['node_expire_patterns_container'][$name]['node_expire_patterns_' . $name . '_type'] = array(
      '#type'          => 'select',
      '#title'         => t('Action to do'),
      '#default_value' => $handler->getNodeTypeActionType($num),
      '#options'       => $options,
      '#description'   => t('Action to do.'),
      '#states' => $states,
    );
    $form['workflow']['node_expire_container']['node_expire_patterns_container'][$name]['node_expire_patterns_' . $name . '_tag'] = array(
      '#title' => t('Tag'),
      '#description' => t('Tag value.'),
      '#type' => 'textfield',
      '#size' => 60,
      '#default_value' => $handler->getNodeTypeActionTag($num),
      '#states' => $states,
    );
  }

  /**
   * Implements hook_form_alter().
   *
   * Adds expiration options to the node entry forms.
   */
  public static function hookFormAlter(&$form, &$form_state, $form_id) {
    if ((isset($form['type'])) &&
      (isset($form['type']['#value']) && is_string($form['type']['#value'])) &&
      ($form['type']['#value'] . '_node_form' == $form_id) &&
      ($ntypes = variable_get('node_expire_ntypes', array())) &&
      (isset($ntypes[$form['type']['#value']])) &&
      ($ntype = $ntypes[$form['type']['#value']])) {
      self::doFormAlter($ntype, $form, $form_state, $form_id);
    }
  }

  /**
   * Implements hook_form_alter().
   *
   * Adds expiration options to the node entry forms.
   */
  private static function doFormAlter(&$ntype, &$form, &$form_state, $form_id) {

//    watchdog('debug', '<pre>'. print_r($ntype, TRUE) .'</pre>');

    // Check if the Node expire feature is enabled for the node type.
    $node = isset($form['#node']) ? $form['#node'] : NULL;

//    watchdog('debug', '<pre>'. print_r($node, TRUE) .'</pre>');

    $handle_content_expiry = ConfigHandlerBase::getHandleContentExpiry();
    if ($handle_content_expiry != 0) {
      if (empty($ntype['enabled'])) {
        return;
      }
    }

    if (!user_access('edit node expire')) {
      return;
    }

    // Don't show actions history for new unsaved nodes
    if (!isset($node->nid)) {
      return;
    }

    // Don't show actions history for node without expire value
    if (empty($node->expire)) {
      return;
    }

    $fieldset = array(
      '#type'  => 'fieldset',
      '#title' => t('Actions history'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $node_actions = ActionsHandler::getActionsSnapshot($node);
//    ModuleUtils::debug($node_actions);

    foreach($node_actions as $na) {
      $s = self::getNodeActionString($na);

      $action_field = array(
        '#markup' => $s,
      );
      $fieldset[$na->number] = $action_field;
    }


//    $action_field = array(
//      '#title' => t('Expiration date'),
//      '#type' => 'textfield',
//      '#maxlength' => 25,
//      '#default_value' => 'Test',
//    );

//    $action_field = array(
//      '#markup' => t('Test'),
//    );
//    $fieldset['action'] = &$action_field;
//
//    watchdog('debug', '<pre>'. print_r($fieldset, TRUE) .'</pre>');


    // Use form components from node_expire hook.
    if (isset($form['options1'])) {
      $form['options1']['actions'] = &$fieldset;
    }
    else {
      $form['options']['actions'] = &$fieldset;
    }


//    // If we use hidden value, do not create fieldset.
//    if ($expire_field['#type'] == 'value') {
//      $form['options1'] = array();
//      $form['options1']['expire'] = &$expire_field;
//    }
//    // If the form doesn't have the publishing options we'll create our own.
//    elseif (!$form['options']['#access']) {
//      $form['options1'] = array(
//        '#type' => 'fieldset',
//        '#title' => t('Publishing options'),
//        '#collapsible' => TRUE,
//        '#collapsed' => FALSE,
//        '#weight' => 95,
//      );
//      $form['options1']['expire'] = &$expire_field;
//    }
//    else {
//      $form['options']['expire'] = &$expire_field;
//    }
//
//    if (isset($node->expired)) {
//      $form['node_expire'] = array(
//        '#type' => 'value',
//        '#value' => TRUE,
//      );
//    }

  }

  /**
   * Implements hook_form_alter().
   *
   */
  private static function getNodeActionString(NodeAction $na) {

    $s = '';

    $s .= $na->number . '. ';
    $s .= $na->name . ', ' . t('to be executed') . ' ';
    $s .= TimestampUtils::timestampToStr($na->timestampToDo) . ', ';

    if ($na->timestampDone==0) {
      $s .= t('not done yet.');
    }
    else {
      $s .= t('executed') . ' ' .
        TimestampUtils::timestampToStr($na->timestampDone) . ', ';
    }

    $s .= '<br />';

    return $s;

  }

}
