<?php

/**
 * @file
 * RulesHookHandler class.
 */

namespace Drupal\node_expire_patterns\Module\Hook;

/**
 * RulesHookHandler class.
 */
class RulesHookHandler {

  /**
   * Implements hook_rules_event_info().
   *
   * @ingroup rules
   */
  public static function hookRulesEventInfo() {
    $events['node_expire_patterns_action'] = array(
      'label' => t('content expire action'),
      'group' => t('Node'),
      'variables' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node'),
        ),
        'uuid' => array(
          'type' => 'text',
          'label' => t('Action UUID'),
        ),
        'tag' => array(
          'type' => 'text',
          'label' => t('Action tag'),
        ),
      ),
    );
    return $events;
  }

}
