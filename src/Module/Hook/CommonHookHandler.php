<?php

/**
 * @file
 * CommonHookHandler class.
 */

namespace Drupal\node_expire_patterns\Module\Hook;

use Drupal\node_expire_patterns\Module\Actions\ActionsHandler;
use Drupal\node_expire_patterns\Module\Config\NodeTypesConfigHandler;

/**
 * CommonHookHandler class.
 */
class CommonHookHandler {

  /**
   * Implements hook_cron().
   */
  public static function hookCron() {
//    $handle_content_expiry = ConfigHandler::getHandleContentExpiry();
//    if ($handle_content_expiry != 2) {
//      $result = DbHandler::selectExpired();
//    }
//    else {
//      $result = DbHandler::selectExpiredNonFlagged();
//    }
//    // $nids = array();
//    // Create NodeTypesConfigHandler once for whole cycle.
//    $configHandler = new NodeTypesConfigHandler();
//    foreach ($result as $record) {
//      // Try to handle the record.
//      try {
//        // $nids[] = $record->nid;
//        $node_type = $record->type;
//        $action_type = $configHandler->getActionType($node_type);
//        $nid = $record->nid;
//        DbHandler::setExpired($nid);
//        // $node = node_load($record->nid);
//        // rules_invoke_event('node_expired', $node);
//        ActionsHandler::doAction($action_type, $nid);
//      }
//      catch (\Exception $e) {
//        // TODO: Add configurable logging.
//      }
//    }

    // Cycle by Node Types
    // Inside cycle by actions

    // Create NodeTypesConfigHandler once for whole processing.
    $configHandler = new NodeTypesConfigHandler();
    $actions = $configHandler->getActionsList();

    ActionsHandler::handleActionsList($actions);

  }

}
