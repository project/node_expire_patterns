<?php

/**
 * @file
 * FormHookHelper class.
 */

namespace Drupal\node_expire_patterns\Module\Hook;

use Drupal\node_expire_patterns\Module\Utils\ModuleUtils;


/**
 * FormHookHelper class.
 */
class FormHookHelper {


  /**
   * Implements hook_form_alter().
   */
//  public static function doFormNodeTypeFormAlterValidate(&$form, &$form_state) {
//    $node_expire = &$form_state['values']['node_expire'];
//    if (!empty($node_expire) and !strtotime($node_expire)) {
//      form_set_error('node_expire',
//        t('This values should be in PHP <a href="http://www.php.net/strtotime" target="_blank">strtotime format</a>.'));
//    }
//
//    $node_expire_max = &$form_state['values']['node_expire_max'];
//    if (!empty($node_expire_max)) {
//      if (!strtotime($node_expire_max)) {
//        form_set_error('node_expire_max',
//          t('This values should be in PHP <a href="http://www.php.net/strtotime" target="_blank">strtotime format</a>.'));
//      }
//      elseif (strtotime($node_expire) > strtotime($node_expire_max)) {
//        form_set_error('node_expire_max',
//          t('This value cannot be earlier then the default value.'));
//      }
//    }
//
//    $node_expire_required = &$form_state['values']['node_expire_required'];
//    if (!empty($node_expire_required)) {
//      if (empty($node_expire)) {
//        form_set_error('node_expire',
//          t('Default expiration date should be set with expiration date required.'));
//      }
//    }
//  }

  /**
   * Implements hook_form_alter().
   */
  public static function doFormNodeTypeFormAlterSubmit(&$form, &$form_state) {

    $ntypes = variable_get('node_expire_patterns_ntypes', array());
    $type = $form_state['values']['type'];

    $enabled = $form_state['values']['node_expire_patterns_enabled'];
    $ntypes[$type]['enabled'] = $enabled;

    if ($enabled) {
      $cnt = $form_state['values']['node_expire_patterns_actions_count'];
      $ntypes[$type]['actions_count'] = $cnt;
      for ($i = 1; $i <= $cnt; $i++) {
        $ntypes[$type]['actions'][$i]['order'] = $i;
        $ntypes[$type]['actions'][$i]['uuid'] = $form_state['values']['node_expire_patterns_action' . $i . '_uuid'];
        $ntypes[$type]['actions'][$i]['name'] = $form_state['values']['node_expire_patterns_action' . $i . '_name'];
        $ntypes[$type]['actions'][$i]['weight'] = $form_state['values']['node_expire_patterns_action' . $i . '_weight'];
        $ntypes[$type]['actions'][$i]['type'] = $form_state['values']['node_expire_patterns_action' . $i . '_type'];
        $ntypes[$type]['actions'][$i]['interval'] = $form_state['values']['node_expire_patterns_action' . $i . '_interval'];
        $ntypes[$type]['actions'][$i]['tag'] = $form_state['values']['node_expire_patterns_action' . $i . '_tag'];
      }

      // Reorder with weight.
      $array = $ntypes[$type]['actions'];
      // http://stackoverflow.com/questions/6053994/using-usort-in-php-with-a-class-private-function
      usort($array, array('Drupal\node_expire_patterns\Module\Hook\FormHookHelper','cmp'));

      $arr = array();
      $j = 1;
      foreach($array as $key=>$value) {
        $value['order'] = $j;
        $arr[$j] = $value;
        $j++;
      }

//      ModuleUtils::debug($arr);
      $ntypes[$type]['actions'] = $arr;

    }

    variable_set('node_expire_patterns_ntypes', $ntypes);

    ModuleUtils::doVariablesCleanup();
  }

  private static function cmp($a, $b)
  {
    return strcmp($a["weight"] . '***' . $a["order"],
      $b["weight"] . '***' . $b["order"]);
  }

}
