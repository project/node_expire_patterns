<?php

/**
 * @file
 * ActionsHandler class.
 */

namespace Drupal\node_expire_patterns\Module\Actions;

use Drupal\node_expire\Module\CommonExpire\Actions\ActionTypeEnum;
use Drupal\node_expire\Module\CommonExpire\Conversion\TimestampHelper;
use Drupal\node_expire\Module\CommonExpire\Exception\ExpireException;
use Drupal\node_expire\Module\CommonExpire\Node\NodeHelper;
use Drupal\node_expire_patterns\Module\Config\NodeTypeConfigHandler;
use Drupal\node_expire_patterns\Module\Db\DbHandler;

/**
 * ActionsHandler class.
 */
class ActionsHandler {

  public static function handleActionsList($actions) {
    foreach($actions as $action) {
      self::handleAction($action);
    }
  }

  public static function handleAction(Action $action) {

    // Get date/time(timestamp), which will be compared with
    // node expiry date/time. Calculation of it before query allows to
    // make faster DB queries without adding interval for each database record.
    // Instead os adding interval to each $node->expire
    // we just subtract it from $timestamp parameter.
//     watchdog('debug', '<pre>'. print_r($action, TRUE) .'</pre>');
//     watchdog('debug', '<pre>'. print_r(REQUEST_TIME, TRUE) .'</pre>');
    $timestamp = TimestampHelper::subtractInterval($action->interval, REQUEST_TIME);
//     watchdog('debug', '<pre>'. print_r($timestamp, TRUE) .'</pre>');
    $records = DbHandler::selectForAction($action, $timestamp);
    foreach($records as $record) {
      self::handleActionRecord($action, $record);
    }

  }

  public static function handleActionRecord(Action $action, $record) {

    $nid = $record->nid;
    self::doAction($action, $nid);

    // OK. // TODO: Add different status values.
    $status = 1;
    // And add tracking record about the action.
    DbHandler::insertActionRecord($action, $nid, $status);
  }

  /**
   * Executes particular action with the node.
   *
   * Somehow similar to the function in ActionsHandler from base
   * module Node Expire.
   *
   */
  public static function doAction(Action $action, $nid) {

    switch ($action->type) {
      case ActionTypeEnum::NONE:
        // Do nothing.
        break;

      case ActionTypeEnum::RULES_EVENT:
        if (module_exists('rules')) {
          $node = node_load($nid);
          rules_invoke_event('node_expire_patterns_action', $node,
            $action->uuid, $action->tag);
        }
        else {
          throw new ExpireException(t('Module Rules is not installed.'));
        }
        break;

      case ActionTypeEnum::NODE_PUBLISH:
        NodeHelper::publishNode($nid);
        break;

      case ActionTypeEnum::NODE_UNPUBLISH:
        NodeHelper::unpublishNode($nid);
        break;

      case ActionTypeEnum::NODE_STICKY:
        NodeHelper::makeNodeSticky($nid);
        break;

      case ActionTypeEnum::NODE_UNSTICKY:
        NodeHelper::makeNodeUnsticky($nid);
        break;

      case ActionTypeEnum::NODE_PROMOTE_TO_FRONT:
        NodeHelper::promoteNode($nid);
        break;

      case ActionTypeEnum::NODE_REMOVE_FROM_FRONT:
        NodeHelper::unpromoteNode($nid);
        break;

      default:
        // Do nothing.
        break;
    }

  }

  /**
   * Returns actions snapshot.
   *
   * Actions snapshot means list of all actions, done and
   * going to be done.
   *
   */
  public static function getActionsSnapshot($node) {

    if (!isset($node->nid)) {
      return array();
    }

    // Get actions list for the node type.
    $config_handler = new NodeTypeConfigHandler($node->type);
    $actions = $config_handler->getNodeTypeActions();

    // Create node actions for particular node as non-executed
    // and indexed by Action UUID.
    $node_actions = array();
    foreach($actions as $action) {
      $na = NodeAction::getByActionAndNode($action, $node);
      $node_actions[$action->uuid] = $na;
    }

    // Fill up executed actions from database.
    $records = DbHandler::selectActionsByNode($node->nid);
    foreach($records as $record) {
      if (isset($node_actions[$record->action_uuid]))
      {
        $node_actions[$record->action_uuid]->status = $record->action_status;
        $node_actions[$record->action_uuid]->timestampDone =
          $record->action_timestamp;
      }
    }

//    watchdog('debug', '<pre>'. print_r($node_actions, TRUE) .'</pre>');

    return $node_actions;

  }

}
