<?php

/**
 * @file
 * NodeAction class.
 */

namespace Drupal\node_expire_patterns\Module\Actions;

use Drupal\node_expire\Module\CommonExpire\Conversion\TimestampHelper;
use Drupal\node_expire\Module\Utils\TimestampUtils;


/**
 * NodeAction class.
 *
 * Represents Action for particular node.
 *
 */
class NodeAction extends Action {

  public $nid;
  public $status;

  public $timestampToDo;
  public $timestampDone = 0;

  public static function getByAction(Action $action) {

    $na = new NodeAction();

    $na->nodeType = $action->nodeType;
    $na->uuid = $action->uuid;

    $na->number = $action->number;
    $na->name = $action->name;
    $na->weight = $action->weight;
    $na->type = $action->type;
    $na->interval = $action->interval;
    $na->tag = $action->tag;

    return $na;

  }

  public static function getByActionAndNode(Action $action, $node) {

    $na = self::getByAction($action);
    $na->nid = $node->nid;
    // Default value.
    $na->status = ActionStatusEnum::NOt_STARTED;

    $expire = TimestampUtils::dateStrToDb($node->expire, $node->type);
    $timestamp = TimestampHelper::addInterval($action->interval, $expire);
    $na->timestampToDo = $timestamp;

    return $na;

  }

}
