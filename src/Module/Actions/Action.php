<?php

/**
 * @file
 * Action class.
 */

namespace Drupal\node_expire_patterns\Module\Actions;

/**
 * Action class.
 */
class Action {

  public $nodeType;
  public $uuid;

  public $number;
  public $name;
  public $weight;
  public $type;
  public $interval;
  public $tag;

}
