<?php

/**
 * @file
 * ActionStatusEnum class.
 */

namespace Drupal\node_expire_patterns\Module\Actions;

/**
 * ActionStatusEnum class.
 */
class ActionStatusEnum {
  const NOt_STARTED = 0;
  const SUCCESS = 1;
}
