<?php

/**
 * @file
 * NodeTypeConfigHandler class.
 */

namespace Drupal\node_expire_patterns\Module\Config;
use Drupal\node_expire_patterns\Module\Actions\Action;


/**
 * NodeTypeConfigHandler class.
 *
 * Defines config handler for different node types.
 */
class NodeTypeConfigHandler  {

  /**
   * Node type.
   *
   * @var string
   */
  protected $nodeType;

//  /**
//   * @return string
//   */
//  public function getNodeType()
//  {
//    return $this->nodeType;
//  }
//
//  /**
//   * @param string $nodeType
//   */
//  public function setNodeType($nodeType)
//  {
//    $this->nodeType = $nodeType;
//  }

  /**
   * Period.
   *
   * @var array
   */
  protected $nodeTypeConfigs;

  /**
   * Constructs a NodeTypeConfigHandler object.
   *
   * @param string $node_type
   *   Node type.
   */
  public function __construct($node_type) {
    $this->nodeType = $node_type;
    $this->loadConfigs();
  }

  /**
   * Loads configuration array.
   */
  protected function loadConfigs() {
    $this->nodeTypeConfigs = variable_get('node_expire_patterns_ntypes', array());
  }

//  /**
//   * Saves configuration array.
//   */
//  public function saveConfigs() {
//    variable_set('node_expire_patterns_ntypes', $this->nodeTypeConfigs);
//  }
//


  /**
   * Returns Enabled for current node type.
   */
  public function getNodeTypeEnabled() {
    if (!empty($this->nodeTypeConfigs[$this->nodeType]['enabled'])) {
      $val = TRUE;
    }
    else {
      $val = FALSE;
    }
    return $val;
  }

  /**
   * Returns actions count for current node type.
   */
  public function getNodeTypeActionsCount() {
    if (!empty($this->nodeTypeConfigs[$this->nodeType]['actions_count'])) {
      $val = $this->nodeTypeConfigs[$this->nodeType]['actions_count'];
    }
    else {
      $val = 0;
    }
    return $val;
  }

  /**
   * Returns action order for current node type and action number.
   */
  public function getNodeTypeActionOrder($num) {
    if (!empty($this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['order'])) {
      $val = $this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['order'];
    }
    else {
      $val = 0;
    }
    return $val;
  }

  /**
   * Returns action weight for current node type and action number.
   */
  public function getNodeTypeActionWeight($num) {
    if (!empty($this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['weight'])) {
      $val = $this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['weight'];
    }
    else {
      $val = 0;
    }
    return $val;
  }

  /**
   * Returns action name for current node type and action number.
   */
  public function getNodeTypeActionUuid($num) {
    if (!empty($this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['uuid'])) {
      $val = $this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['uuid'];
    }
    else {
      $val = uuid_generate();
    }
    return $val;
  }

  /**
   * Returns action name for current node type and action number.
   */
  public function getNodeTypeActionName($num) {
    if (!empty($this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['name'])) {
      $val = $this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['name'];
    }
    else {
      $val = t('Action ') . $num;
    }
    return $val;
  }

  /**
   * Returns action type for current node type and action number.
   */
  public function getNodeTypeActionType($num) {
    if (!empty($this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['type'])) {
      $val = $this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['type'];
    }
    else {
      $val = 0;
    }
    return $val;
  }

  /**
   * Returns action interval for current node type and action number.
   */
  public function getNodeTypeActionInterval($num) {
    if (!empty($this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['interval'])) {
      $val = $this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['interval'];
    }
    else {
      $val = t('- 0 days');
    }
    return $val;
  }

  /**
   * Returns action tag for current node type and action number.
   */
  public function getNodeTypeActionTag($num) {
    if (!empty($this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['tag'])) {
      $val = $this->nodeTypeConfigs[$this->nodeType]['actions'][$num]['tag'];
    }
    else {
      $val = t('Tag ') . $num;
    }
    return $val;
  }


  /**
   * Returns default node type config.
   */
  protected function getNodeTypeConfigDefault() {
    $config_default = array(
//      // Current date.
//      'update_expiry_start'  => 1,
//      // Year.
//      'update_expiry_interval' => 4,
//      'update_expiry_multiplier' => 1,
    );

    return $config_default;
  }

  /**
   * Returns single node type config.
   */
  public function getNodeTypeConfig() {
    if (isset($this->nodeTypeConfigs[$this->nodeType])) {
      return $this->nodeTypeConfigs[$this->nodeType];
    }
    else {
      return $this->getNodeTypeConfigDefault();
    }
  }

  /**
   * Returns list of single node type actions.
   */
  public function getNodeTypeActions() {

    $config = $this->getNodeTypeConfig();

    $actions = array();

    if (is_array($config)) {
      if (isset($config['enabled']) && $config['enabled'] == 1) {
        if (isset($config['actions_count'])
          && filter_var($config['actions_count'], FILTER_VALIDATE_INT)) {
          $actions_count = $config['actions_count'];
          if (isset($config['actions']) && is_array($config['actions'])) {
            for ($i = 1; $i <= $actions_count; $i++) {
              $arr = $config['actions'][$i];
              $action = new Action();

              $action->nodeType = $this->nodeType;
              $action->uuid = $arr['uuid'];
              $action->number = $i;
              $action->name = $arr['name'];
              $action->weight = $arr['weight'];
              $action->type = $arr['type'];
              $action->interval = $arr['interval'];
              $action->tag = $arr['tag'];

              $actions[] = $action;

            }
          }
        }
      }
    }

    // TODO: Add $actions[] reordering by weight.

//    watchdog('debug', '<pre>'. print_r($actions, TRUE) .'</pre>');

    return $actions;

  }

//  /**
//   * Returns update expiry start.
//   */
//  public function getUpdateExpiryStart() {
//    $config = $this->getNodeTypeConfig();
//    if (isset($config['update_expiry_start'])) {
//      return $config['update_expiry_start'];
//    }
//    else {
//      return 1;
//    }
//  }
//
//  /**
//   * Returns update expiry interval.
//   */
//  public function getUpdateExpiryInterval() {
//    $config = $this->getNodeTypeConfig();
//    if (isset($config['update_expiry_interval'])) {
//      return $config['update_expiry_interval'];
//    }
//    else {
//      return 4;
//    }
//  }
//
//  /**
//   * Returns update expiry multiplier.
//   */
//  public function getUpdateExpiryMultiplier() {
//    $config = $this->getNodeTypeConfig();
//    if (isset($config['update_expiry_multiplier'])) {
//      return $config['update_expiry_multiplier'];
//    }
//    else {
//      return 1;
//    }
//  }

}
