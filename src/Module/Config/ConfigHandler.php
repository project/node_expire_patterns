<?php

/**
 * @file
 * ConfigHandler class.
 */

namespace Drupal\node_expire_patterns\Module\Config;

/**
 * ConfigHandler class.
 */
class ConfigHandler {

  /**
   * Returns ActionsCount configuration value.
   */
  public static function getActionsCount($node_type) {
    $val = variable_get('node_expire_patterns_actions_count_' . $node_type, 0);
    return $val;
  }

}
