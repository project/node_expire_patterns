<?php

/**
 * @file
 * NodeTypesConfigHandler class.
 */

namespace Drupal\node_expire_patterns\Module\Config;

use Drupal\node_expire_patterns\Module\Actions\Action;


/**
 * NodeTypesConfigHandler class.
 *
 * Defines config handler for node types.
 *
 * This is NodeTypesConfigHandler class for node_expire_patterns module.
 * Differ it from NodeTypesConfigHandler class for node_expire module.
 */
class NodeTypesConfigHandler {

  /**
   * Array with configs for node types.
   *
   * @var array
   */
  protected $nodeTypeConfigs;

  /**
   * Constructs a NodeTypesConfigHandler object.
   */
  public function __construct() {
    $this->loadConfig();
  }

  /**
   * Loads configuration array.
   */
  public function loadConfig() {
    $this->nodeTypeConfigs = variable_get('node_expire_patterns_ntypes', array());
  }

  /**
   * Saves configuration array.
   */
  public function saveConfig() {
    variable_set('node_expire_patterns_ntypes', $this->nodeTypeConfigs);
  }

  /**
   * Returns single node type config.
   *
   * @param string $node_type
   *   Node type.
   */
  public function getNodeTypeConfig($node_type) {
    if ((isset($this->nodeTypeConfigs[$node_type]))
      and ($n_type_config = $this->nodeTypeConfigs[$node_type])) {
      return $n_type_config;
    }
    else {
      return $this->getNodeTypeConfigDefault();
    }
  }

  /**
   * Returns default node type config.
   */
  protected function getNodeTypeConfigDefault() {
    $config_default = array(
      'enabled' => 0,
      'action_count' => 0,
      'actions' => array(),
    );

    return $config_default;
  }

  public function getActionsList() {

    $actions = array();

    foreach($this->nodeTypeConfigs as $key=>$value) {
      if (is_array($value)) {
        if (isset($value['enabled']) && $value['enabled'] == 1) {
          if (isset($value['actions_count'])
            && filter_var($value['actions_count'], FILTER_VALIDATE_INT)) {
//            watchdog('node_expire_patterns', 'Key @key Value @value',
//              array('@key' => $key, '@value' => json_encode($value)),
//              WATCHDOG_DEBUG);
            $actions_count = $value['actions_count'];
            if (isset($value['actions']) && is_array($value['actions'])) {
              for ($i = 1; $i <= $actions_count; $i++) {
                $arr = $value['actions'][$i];
                $action = new Action();

                $action->nodeType = $key;
                $action->uuid = $arr['uuid'];
                $action->number = $i;
                $action->name = $arr['name'];
                $action->weight = $arr['weight'];
                $action->type = $arr['type'];
                $action->interval = $arr['interval'];
                $action->tag = $arr['tag'];

                $actions[] = $action;

              }
            }
          }
        }
      }
    }

    // TODO: Add $actions[] reordering by weight.

//    watchdog('node_expire_patterns', 'Actions @value',
//      array('@value' => json_encode($actions)),
//      WATCHDOG_DEBUG);

    return $actions;
  }

//  /**
//   * Returns action type.
//   *
//   * @param string $node_type
//   *   Node type.
//   */
//  public function getActionType($node_type) {
//    $config = $this->getNodeTypeConfig($node_type);
//    if (isset($config['action_type'])) {
//      $action_type = $config['action_type'];
//    }
//    else {
//      $action_type = 0;
//    }
//
//    return $action_type;
//  }

}
