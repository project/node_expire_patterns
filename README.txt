NODE EXPIRE PATTERNS
====================

Node expire patterns is an addition to Node expire module.
It is performing multiple various actions with different time shift against
node expiry date. As in the example:

Publishing an ad use-case.
- Publish an ad (node) (3 months before expiry).
- Send 1-st warning email with offering to extend the ad (1 week before expiry).
- Send 2-nd warning email with offering to extend the ad (2 days before expiry).
- Unpublish the ad and send confirmation email with offering
  to extend the ad (on expiry).
- Send a follow-up email with offering to extend the ad (1 week after expiry).

Announcing an event use-case.
- Promote an event (node) to front page (2 months before event date - expiry).
- Send 1-st notification email (2 weeks before expiry).
- Send 2-nd notification email (3 days before expiry).
- Send 3-rd notification email (1 day before expiry).
- Unpublish the event (on expiry).

Creating an appointment use-case.
 - Send 1-st notification email (2 weeks before appointment date/time - expiry).
 - Send 2-nd notification email (3 days before expiry).
 - Send 3-rd notification email (1 day before expiry).
 - Unpublish the appointment (on expiry).

To use the module:
 - Install and enable the module.
   (Required modules:
   Node expire >= 7.x-2.2
   X Autoload >=7.x-5.0
   Uuid)
 - In Administration -> Structure -> Content types -> {Your content type} ->
   Publishing options
   - Set checkbox "Enable Node Expire Patterns".
   - Set necessary Actions count.
   - For each Action set:
     - Human-readable name.
     - Weight (initially keep it 0, it can be used to reorder Actions,
       if necessary).
     - Interval in relation to Expiry date.
     - Action to do.
     - Tag. This is an arbitrary action-related string and has no relation
       to taxonomy.
   - Also UUID is automatically set to each Action.
   - UUID and Tag of the Action are passed to Rules event
     "content expire action" as parameters.
 - Run cron and see how actions are executed.
 - For each Node, where Node Expire Patterns are enabled and Expiry date
   is set and saved, Actions History is shown in Publishing options,
   something like this:

   Actions history
   1. Action 1, to be executed 02/19/2016 23:05, executed 02/19/2016 23:20,
   2. Action 2, to be executed 02/20/2016 23:05, executed 02/20/2016 23:23,
   3. Action 3, to be executed 02/21/2016 23:05, not done yet.


